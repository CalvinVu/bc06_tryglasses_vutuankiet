import React, { Component } from "react";
import { dataGlass } from "./DataGlass";

export default class ExGlass extends Component {
  image = "./glassesImage/model.jpg";
  state = {
    tryGlass: dataGlass[0].url,
  };
  changeGlass = (number) => {
    this.setState({
      tryGlass: number,
    });
  };
  renderDataGlass = () => {
    let inforGlass = dataGlass.map((item) => {
      return (
        <div className="card col-1" style={{ width: "10rem" }}>
          <button
            onClick={() => {
              this.changeGlass(item.url);
            }}
            className="btn btn-white"
          >
            <img style={{ height: "20px" }} src={item.url}></img>
          </button>
        </div>
      );
    });
    return inforGlass;
  };
  render() {
    return (
      <div
        className=""
        style={{
          height: "100vh",
          backgroundImage: "url(" + "./glassesImage/background.jpg" + ")",
          backgroundPosition: "center",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div className="row">{this.renderDataGlass()}</div>
        <div>
          <div className="position-relative">
            <img className="" src={this.image}></img>
          </div>
          <div
            className="position-absolute center"
            style={{ width: "65%", top: "190px", right: "255px" }}
          >
            <img style={{ width: "30%" }} src={this.state.tryGlass}></img>
          </div>
        </div>
      </div>
    );
  }
}
